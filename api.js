const db = require("./db")

const {
    GetItemCommand,
    PutItemCommand,
    UpdateItemCommand,
    DeleteItemCommand
} = require("@aws-sdk/client-dynamodb");

const { marshall, unmarshall } = require("@aws-sdk/util-dynamodb");

const getPost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const params = {
            TableName: process.env.DYNAMODB_TABLE_EXAMPLE,
            Key: marshall({ postId: event.pathParameters.postId })
        }
        const { Item } = await db.send(new GetItemCommand(params));
        response.body = JSON.stringify({
            message: "Successfully retrieved post.",
            statusCode: response.statusCode,
            data: (Item) ? unmarshall(Item) : {}
        });
    } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.body =JSON.stringify({
            message: "Failed to get post.",
            errorMsg: error.message,
            errorStack: error.stack
        })
    }

    return response;
};

const createPost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const body = JSON.parse(event.body);
        const params = {
            TableName: process.env.DYNAMODB_TABLE_EXAMPLE,
            Item: marshall(body || {})
        };
        const createResult = await db.send(new PutItemCommand(params));

        response.body = JSON.stringify({
            message: "Successfully created post.",
            createResult
        });
    } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.body =JSON.stringify({
            message: "Failed to create post.",
            errorMsg: error.message,
            errorStack: error.stack
        });
    }

    return response;
};

const updatePost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const body = JSON.parse(event.body);
        const objKeys = Object.keys(body);

        const params = {
            TableName: process.env.DYNAMODB_TABLE_EXAMPLE,
            Key: marshall({ postId: event.pathParameters.postId }),
            UpdateExpression: `SET ${objKeys.map((_, index) => `#Key${index} = :value${index}`).join(", ")}`,
            ExpressionAttributeNames: objKeys.reduce((acc, key, index) => ({
                ...acc,
                [`#Key${index}`]: key
            }), {}),
            ExpressionAttributeValues: marshall(objKeys.reduce((acc, key, index) => ({
                ...acc,
                [`:value${index}`]: body[key]
            }), {}))
        };
        const updateResult = await db.send(new UpdateItemCommand(params));

        response.body = JSON.stringify({
            message: "Successfully updated post.",
            updateResult
        });
    } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.body =JSON.stringify({
            message: "Failed to update post.",
            errorMsg: error.message,
            errorStack: error.stack
        })
    }

    return response;
};

const deletePost = async (event) => {
    const response = { statusCode: 200 }

    try {
        const params = {
            TableName: process.env.DYNAMODB_TABLE_EXAMPLE,
            Key: marshall({ postId: event.pathParameters.postId })
        };
        const deleteResult = await db.send(new DeleteItemCommand(params));

        response.body = JSON.stringify({
            message: "Successfully deleted post.",
            deleteResult
        });
    } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.body =JSON.stringify({
            message: "Failed to delete post.",
            errorMsg: error.message,
            errorStack: error.stack
        })
    }

    return response;
};

module.exports = {
    getPost,
    createPost,
    updatePost,
    deletePost
};