const db = require("../db")

const {
    GetItemCommand,
    PutItemCommand,
    UpdateItemCommand,
    DeleteItemCommand
} = require("@aws-sdk/client-dynamodb");

const { marshall, unmarshall } = require("@aws-sdk/util-dynamodb");
const { v4: uuidv4 } = require('uuid');

const getDestination = async (event) => {
    const response = { statusCode: 200 }

    try {
        const params = {
            TableName: 'DestinationTable',
            Key: marshall({ postId: event.pathParameters.postId })
        }
        const { Item } = await db.send(new GetItemCommand(params));
        console.log({ Item });
        response.body = JSON.stringify({
            message: "El destino fue creado correctamente.",
            data: (Item) ? unmarshall(Item) : {},
            rawData: Item
        });
    } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.body =JSON.stringify({
            message: "Failed to get post.",
            errorMsg: error.message,
            errorStack: error.stack
        })
    }

    return response;
};

const createDestination = async (event) => {
    const response = { statusCode: 200 }

    try {
        const body = JSON.parse(event.body);
        const params = {
            TableName: 'DestinationTable',
            Item: marshall({
                idDestination:  uuidv4(),
                ...body
            })
        };
        const createResult = await db.send(new PutItemCommand(params));

        response.body = JSON.stringify({
            message: "El destino fue creado correctamente.",
            createResult
        });
    } catch (error) {
        console.error(error);
        response.statusCode = 500;
        response.body =JSON.stringify({
            message: "Error al crear destino.",
            errorMsg: error.message,
            errorStack: error.stack
        });
    }

    return response;
};

module.exports = {
    getDestination,
    createDestination
};